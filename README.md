# IB Bank Mandiri Telegram Watcher Bot

A [Telegram](https://telegram.org/) bot to watch over transactions that happen in a Mandiri account.

## How does this works?

* In the server-side, the bot periodically fetches data from IB site and checks for new entries.
* The new entries are then pushed to the user through Telegram Bot.

## What's needed?

* NodeJS >= v6.10
* Full-working [CasperJS](http://casperjs.org/), needed to scrape/crawl the site.
* MongoDB, for storing transaction entries.
* Telegram Bot token and user ID.
* HTTPS-enabled web server.
* Of course, internet-banking-enabled Mandiri account.

## Steps to install

* Install NodeJS
* Clone this repository
* Install required dependencies (through `npm install`)
* Install CasperJS (and PhantomJS)
* [Set up the Telegram Bot](https://core.telegram.org/bots) and the web server.
