const casper = require('casper').create({
    pageSettings: {
        loadPlugins: true,
        localToRemoteUrlAccessEnabled: true,
        userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
    },
    verbose: false,
    exitOnError: false,
	waitTimeout: 30000
});

const opts = casper.cli.options;

const TARGET_URL = opts.URL;
const IBM_USERNAME = opts.IUSR;
const IBM_PASSWORD = opts.IPAS;

casper.start(TARGET_URL, function() {
});

casper.waitForText('LOGIN', function () {
    this.fillSelectors('form[name="LoginForm"]', {
        'input[name="userID"]'   : IBM_USERNAME,
        'input[name="password"]' : IBM_PASSWORD
    }, false);

    this.click('input[name="image"][type="image"]');
});

casper.waitFor(function check() {
    return this.getTitle().search(/[Ww]elcome/) != -1;
}, function then() {});

casper.withFrame('CONTENT', function() {
    this.evaluate(function() {
        var form = document.querySelector('#saSelect');
        form.selectedIndex = form.options.length - 1;
        form.onchange();
    });
});

casper.withFrame('CONTENT', function() {
    this.waitForText('MUTASI REKENING', function() {
        this.evaluate(function() {
            var day = document.querySelector('select[name="fromDay"]');
            if (day.value <= 3) {
                var month = document.querySelector('select[name="fromMonth"]');
                if (month.value == 1) {
                    month.value = 12;
                    var year = document.querySelector('select[name="fromYear"]');
					year.value -= 1;
					year.onChange();
                } else {
                    month.value -= 1;
                }
                month.onChange();

                if ([1, 3, 5, 7, 8, 10, 12].indexOf(Number(month.value)) != -1) {
                    day.value = 31 - day.value + 1;
                } else if ([4, 6, 9, 11].indexOf(Number(month.value)) != -1) {
                    day.value = 30 - day.value + 1;
                } else {
                    var year = document.querySelector('select[name="fromYear"]');
                    year = Number(year.value);
                    var isLeap = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
                    if (isLeap) {
                        day.value = 29 - day.value + 1;
                    } else {
                        day.value = 28 - day.value + 1;
                    }
                }
            } else {
                day.value -= 3;
            }
            day.onChange();
        });

        this.click('div#buttonsubmit', "50%", "50%");
    });
});

casper.withFrame('CONTENT', function() {
    this.waitForText('Keterangan Transaksi', function() {
        var data = this.evaluate(function() {
            var tbl = document.querySelector('div[align="center"] > table:nth-of-type(3)');
            var rowData = [];
            for (var i = 1, row; row = tbl.rows[i]; ++i) {
                var res = [];
                for (var j = 0, cell; cell = row.cells[j]; ++j) {
                    var content = cell.innerText.trim();
                    content = content.replace(/\n/g, ' | ');
                    res.push(content);
                }
                res = {
                    date: res[0],
                    desc: res[1],
                    debet: res[2],
                    kredit: res[3]
                };
                rowData.push(res);
            }
            return rowData;
        });
        console.log(JSON.stringify(data));
    });
});


casper.withFrame('top', function() {
    this.click('a[href*="ogout"]');
});

casper.waitForText('Ringkasan Aktivitas', function() {
});

casper.run();
