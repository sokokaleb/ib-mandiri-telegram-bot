require('dotenv').config();
const Telebot = require('telebot');
const schedule = require('node-schedule');
const winston = require('winston');
const moment = require('moment');
const acc = require('accounting');

var logger = new (winston.Logger)({
	transports: [
		new (winston.transports.Console)({
			timestamp: function() {
				return moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss (ZZ)');
			}
		})
	]
});
const scraper = require('./master')(logger);

const bot = new Telebot({
	token: process.env.TELEGRAM_BOT_TOKEN,
	webhook: {
		url: process.env.WEBHOOK_URL,
		host: process.env.WEBHOOK_HOST || '0.0.0.0',
		port: process.env.WEBHOOK_PORT || 8085
	}
});

const writeOpts = {
	parse: 'Markdown'
};

const user_id = Number(process.env.MASTER_ID || -1);
const MASTER_ID = Number(process.env.MASTER_ID || -1);

ispermitted = (u_id, m_id, sender_id) => {
	if (m_id == -1) {
		return u_id != -1 && sender_id == u_id;
	} else {
		return u_id == m_id && sender_id == u_id;
	}
}

parseusr = (msg) => {
	var res = null;
	if (msg.from !== undefined) {
		var fname = msg.from.first_name;
		var lname = msg.from.last_name || '';
		var uname = '';
		if (msg.from.username !== undefined) {
			uname = '(' + msg.from.username + ')';
		}
		var cid = '(' + msg.from.id + ')';
		res = `*${fname + ' ' + lname + ' ' + uname + ' ' + cid}*`;
	}
	return res;
}

ggreport = (msg) => {
	var resp = parseusr(msg);
	resp += ' tries to access your bot.';
	bot.sendMessage(user_id, resp, writeOpts);
}

formatData = (arr) => {
	var response = '';
	for (var i = 0; i < arr.length; ++i) {
		if (i > 0) {
			response += '\n-----\n';
		}
		response += `*${arr[i].date}*\n`; 
		response += `${arr[i].desc}\n`;
		var amt = acc.unformat(arr[i].kredit, ',')
			- acc.unformat(arr[i].debet, ',');
		var fmtamt = acc.formatMoney(amt, {
			precision: 2,
			thousand: '.',
			decimal: ',',
			format: {
				pos : '%v',
				neg : '(%v)',
				zero: ' -NIL- '
			}
		});
		response += `*${fmtamt}*`;
	}
	return response;
}

scrapeplz = (msg, from_scheduler) => {
	if (from_scheduler || ispermitted(user_id, MASTER_ID, msg.from.id)) {
		var param = '';
		if (from_scheduler) {
			param = 'Scheduler kicks in!';
		} else {
			param = 'Force power!';
		}
		logger.info(param);
		scraper.scrape((arr) => {
			arr = arr || [];
			var response = '';
			if (arr.length === 0) {
				response = '*No new entries!*';
			} else {
				response = `*${arr.length} new entries!*\n\n`;
				response += formatData(arr);
			}
			bot.sendMessage(user_id, response, writeOpts);
		});
	} else {
		ggreport(msg);
		var reply_msg = 'Who are you?';
		return bot.sendMessage(msg.from.id, reply_msg);
	}
}

seehistory = (msg) => {
	if (ispermitted(user_id, MASTER_ID, msg.from.id)) {
		var tokens = msg.text.split(' ');
		var hist_num = Number(tokens[1]) || 5;
		hist_num = Math.max(1, hist_num);
		logger.info('Got histhist with param: ' + hist_num);
		scraper.seehistory(hist_num, (arr) => {
			arr = arr || [];
			var response = '';
			if (arr.length === 0) {
				response = '*No history...*';
			} else {
				if (arr.length == 1) {
					response = `*Latest entry:*\n\n`;
				} else {
					response = `*${arr.length} latest entries:*\n\n`;
				}
				response += formatData(arr);
			}
			bot.sendMessage(user_id, response, writeOpts);
		});
	} else {
		ggreport(msg);
		var reply_msg = 'Who are you?';
		return bot.sendMessage(msg.from.id, reply_msg);
	}
}

bot.on('/start', msg => {
	if (!ispermitted(user_id, MASTER_ID, msg.from.id)) {
		ggreport(msg);
	}

	var resp = parseusr(msg);
	if (resp !== null) {
		resp = 'You are ' + resp;
		resp += '\n\n';
	} else {
		resp = '';
	}
	resp += 'Do you know what you\'re doing?';
	return bot.sendMessage(msg.from.id, resp, writeOpts);
});

bot.on('/connect', msg => {
	if (user_id == -1) {
		user_id = msg.from.id;
		var reply_msg = 'Hi, you\'re successfully connected with IBM Scraper Bot.\n';
		reply_msg += `Your chat_id is ${user_id}.`;
		return bot.sendMessage(user_id, reply_msg, writeOpts);
	} else if (!ispermitted(user_id, MASTER_ID, msg.from.id)) {
		ggreport(msg);
		var reply_msg = 'Who are you?';
		return bot.sendMessage(msg.from.id, reply_msg, writeOpts);
	}
});

schedule.scheduleJob('0 0 2,14 * * *', () => scrapeplz(null, true));
bot.on('/forceplz', (msg) => scrapeplz(msg, false));
bot.on('/histhist', (msg) => seehistory(msg));

bot.connect();

logger.info('Done setup. Running...');
