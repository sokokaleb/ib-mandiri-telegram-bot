const spawn = require('child_process').spawn;

const MongoClient = require('mongodb').MongoClient;

const MONGO_URL = 'mongodb://' + process.env.MONGO_HOST + ':'
                  + process.env.MONGO_PORT + '/' + process.env.MONGO_DB;
const MONGO_COLLECTION = process.env.MONGO_COLLECTION;
const CASPERJS_COMMAND = process.env.CASPERJS_COMMAND;

var logger;

var exports = module.exports = (lgr) => {

    logger = lgr;

    return {
        scrape: (callback) => {
            callback = callback || function() {};

            var args = [];
            args.push('./scrape.js');
            args.push(`--URL=${process.env.IBM_URL}`);
            args.push(`--IUSR=${process.env.IBM_USERNAME}`);
            args.push(`--IPAS=${process.env.IBM_PASSWORD}`);

            logger.info('Spawning CasperJS...');
            casper = spawn(CASPERJS_COMMAND, args);

            logger.info(`Opening MongoDB connection to ${MONGO_URL}`);
            MongoClient.connect(MONGO_URL)
                .then((db) => {
                    function get_id(data) {
                        return '(' + data.date + ')'
                            + '(' + data.desc + ')'
                            + '(' + data.debet + ')'
                            + '(' + data.kredit + ')';
                    }

                    function get_promise(data, coll) {
                        var _id = get_id(data);
                        data.ts = (new Date()).getTime();
                        var query = { _id: _id };
                        var sort = [[ '_id', 1 ]];
                        var update = { $setOnInsert: data };
                        var opts = { new: true, upsert: true };

                        var prom = coll.findAndModify(
                                query,
                                sort,
                                update,
                                opts)
                        .then((res) => {
                            if (res.ok != 1) {
                                return {
                                    ok: false
                                };
                            } else {
                                return {
                                    d: data,
                                    ok: true,
                                    upd: res.lastErrorObject.updatedExisting
                                };
                            }
                        });

                        return prom;
                    }

                    var coll = db.collection(MONGO_COLLECTION);
                    var promises = [];
                    var buffer = '';

                    var promisifyData = function(data) {
                        if (Array.isArray(data)) {
                            for (var i = 0; i < data.length; ++i) {
                                if (data[i].date !== undefined && data[i].desc !== undefined) {
                                    promises.push(get_promise(data[i], coll));
                                }
                            }
                        } else {
                            promises.push(get_promise(data, coll));
                        }
                    }

                    casper.stdout.on('data', (data) => {
                        logger.info(`Got data:\n${data}`);
                        try {
                            data = JSON.parse(data);
                            promisifyData(data);
                        } catch (e) {
                            return ;
                        }
                    });

                    casper.on('close', (code) => {
                        if (code !== 0) {
                            logger.warn('Something\'s wrong with CasperJS scraper');
                        } else {
                            if (buffer.length > 0) {
                                try {
                                    buffer = JSON.parse(buffer);
                                    promisifyData(buffer);
                                } catch (e) {
                                    // no op
                                }
                            }
                            Promise.all(promises)
                            .then((res) => {
                                retval = [];
                                for (var i = 0; i < res.length; ++i) {
                                    var obj = res[i];
                                    if (obj.ok && !obj.upd) {
                                        retval.push(obj.d);
                                        logger.info(obj.d);
                                    }
                                }
                                db.close();
                                callback(retval);
                                logger.info('Done writing!');
                            });
                        }
                    });
                })
                .catch((err) => {
                    var e = new Error('Promise error');
                    e.details = err;
                    throw e;
                });
        },
        seehistory: (hist_num, callback) => {
            hist_num = hist_num || 5;
            callback = callback || function () {};
            MongoClient.connect(MONGO_URL)
                .then((db) => {
                    var coll = db.collection(MONGO_COLLECTION);
                    return coll.find({ ts : { '$ne' : null } })
                        .sort({ ts : -1 })
                        .limit(hist_num)
                        .toArray()
                        .then((res) => {
                            res.sort((o1, o2) => o1.ts - o2.ts);
                            logger.info('Succesfully got the data: ' + res);
                            callback(res);
                        });
                })
                .catch((err) => {
                    var e = new Error('Promise error');
                    e.details = err;
                    throw e;
                });
        }
    }
};
